const express = require('express')
const restful = require('node-restful')
const server = express()
const mongoose = restful.mongoose

//Database - faz a conexão com o mongdb
mongoose.Promise = global.Promise //O Promise do mongoose está decrepted, por isso a associação ao o do global
mongoose.connect('mongodb://db/myDb') //db é o nome do servidor e myDb é o nome do schema. Será configurado no compoose

//Teste
server.get('/',(req, res, next) => res.send('Teste no backend')) 

//Starta o Serviço na porta 3000
server.listen(3000)
